#include "file/file.h"

enum status file_open(const char *file_name, FILE **file, char *mode) {
  if (!file) return FILE_OPEN_NULL_ARGUMENT_FILE;
  if (!file_name) return FILE_OPEN_NULL_ARGUMENT_FILE_NAME;
  if (!mode) return FILE_OPEN_NULL_ARGUMENT_MODE;
  *file = fopen(file_name, mode);
  if (*file == NULL) return OPEN_ERROR;
  return SUCCESS;
}

enum status file_close(FILE *file) {
  if (!file) return FILE_CLOSE_NULL_ARGUMENT;
  if (fclose(file) == EOF) return CLOSE_ERROR;
  else return SUCCESS;
}
