#include "../include/format/bmp.h"
#include "../include/file/file.h"
#include "../include/image/image.h"
#include "../include/rotation/rotate.h"
#include "../include/status/status.h"

#include <stdio.h>

// eto zarabotalo!!!!
int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "args error");
        return -1;
    }

    FILE *input_file = {0};
    FILE *output_file = {0};
    
    struct image first_image = (struct image){0};
    
    enum status status = file_open(argv[1], &input_file, "r");
    if (status) {
        printf("%s\n","can not open the file");
        return status;
    }

    status = from_bmp(input_file, &first_image);
    if (status) {
        printf("%s\n","");
        return status;
    }
    
    status = file_close(input_file);
    if (status) {
        printf("%s\n","");
        return status;
    }

    if (first_image.height != 0) {
        struct image second_image = transform(&first_image);
        image_destroy(first_image);

        status = file_open(argv[2], &output_file, "w");
        if (status) {
            printf("%s\n","");
            return status;
        }

        status = to_bmp(output_file, &second_image);
        if (status) {
            printf("%s\n","");
            return status;
        }

        status = file_close(output_file);
        if (status) {
            printf("%s\n","");
            return status;
        }

        image_destroy(second_image);
        printf("%s\n","");
    }
    return 0;
}
