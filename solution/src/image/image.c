#include "../include/image/image.h"
#include <malloc.h>

struct image image_create(const size_t width, const size_t height) {
  struct pixel *data = malloc(width * height * sizeof(struct pixel));
  if (data == NULL) {
    struct image image_null = {0};
    return image_null;
  }

  struct image image = {
            .height = height,
            .width = width,
            .data = data
  };

  return image;
}

void image_destroy(struct image image) {
  free(image.data);
}
