#include "../include/rotation/rotate.h"
#include "../include/image/image.h"
#include <malloc.h>

struct image transform(const struct image *image) {
    if (!image->data) return (struct image) {.width = image->width, .height = image->height, .data = NULL};

    struct image result_image = image_create(image->height, image->width);

    for (size_t y = 0; y < image->height; y++) {
        for (size_t x = 0; x < image->width; x++) {
            result_image.data[get_index(image->height, (x + 1), - ( 1 + y))] = image->data[get_index(image->width, y, x)];
        }
    }
    return result_image;
}
