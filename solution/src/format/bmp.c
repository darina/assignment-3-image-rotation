#include "../include/format/bmp.h"
#include "stdint.h"
#include <stdbool.h>

#define FOUR_BYTES 4
#define ONE_PIXEL 3

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static inline uint8_t get_padding(uint64_t width){
    return (uint8_t)(FOUR_BYTES - (width*ONE_PIXEL) % FOUR_BYTES) % FOUR_BYTES;
}


static bool read_header(FILE *file, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, file);
}

static size_t size_of_file(const size_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

static size_t get_padding_size(size_t width) {
    if (width % 4 == 0) return 0;
    else return (width*3/4 + 1)* 4 - width*3;
}

static size_t size_of_image(const struct image *image) {
    return image->height * (3 * image->width + get_padding_size(image->width));
}

enum status from_bmp(FILE *in, struct image *image) {
    if (!in) return READ_NULL_STREAM;
    struct bmp_header header = {0};

    if (!read_header(in, &header)) {
        return READ_ERROR;
    }
    if(header.bfType != TYPE){
        return READ_INVALID_IMAGE;
    }
    if (header.biBitCount != BIT_COUNT){
        return READ_INVALID_BITS;
    }

if (header.biWidth < 0 || header.biHeight < 0) return READ_INVALID_IMAGE;

    *image = image_create(header.biWidth, header.biHeight);
    
    const long padding = get_padding(image->width);
    
    for (size_t i = 0; i < image->height; ++i) {
        if (fread(image->data + i * image->width, sizeof(struct pixel), image->width, in) != image->width) return READ_INVALID_BITS;
        if (fseek(in, padding, SEEK_CUR)) return READ_INVALID_AMOUNT;
    }

    return SUCCESS;
}

struct bmp_header create_header(const struct image *image) {
    struct bmp_header header = {0};

    header.bfType = 19778;
    header.bfileSize = size_of_file(size_of_image(image));
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = size_of_image(image);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum status to_bmp(FILE *out, const struct image *image) {

    struct bmp_header header = create_header(image);
	
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    fseek(out, header.bOffBits, SEEK_SET);

    const uint8_t padding = get_padding(image->width);
    const uint8_t zero[3] = {0};

    if (image->data) {
        for (size_t i = 0; i < image->height; ++i) {
            if (!fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, out) ||
                !fwrite(zero, padding, 1, out))
                return WRITE_ERROR;
        }    
    } else return WRITE_ERROR;

    return SUCCESS;
}
