#ifndef BMP_H
#define BMP_H

#include "../include/image/image.h"
#include <malloc.h>
#include <status/status.h>
#include  <stdint.h>

#define TYPE 0x4d42
#define BIT_COUNT 24

enum status from_bmp(FILE *in, struct image *image);

enum status to_bmp(FILE *out, const struct image *image);

#endif
