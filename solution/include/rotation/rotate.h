#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "../include/image/image.h"

struct image transform(const struct image *image);

#endif
